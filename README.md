# itsy-bitsy-motor-board #

An ItsyBitsy M4 Express microcontroller is used to drive two DC motors via a DRV8833 motor driver board.

It also reads the wheel encoders of the wheels. 

It exposes a UART interface to drive the motors with simple high level commands.

This repo contains the design for a PCB (in Autodesk Eagle 9.6.2) with headers to connect everything up.

![Wiring](img/front.png)

![Wiring](img/bottom.png)

![final](img/pcb.png)

## Notes

(https://learn.adafruit.com/making-pcbs-with-oshpark-and-eagle/overview)

(https://learn.sparkfun.com/tutorials/how-to-install-and-setup-eagle)

(https://learn.sparkfun.com/tutorials/using-eagle-schematic)

(https://learn.sparkfun.com/tutorials/using-eagle-board-layout)